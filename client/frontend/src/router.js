import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import About from "@/views/About.vue";
import Contact from "@/views/Contact.vue";
import HotelViewing from "@/views/HotelViewing.vue";
import Forms from "@/views/Forms.vue";
import SuperAdmin from "@/views/SuperAdmin.vue";
import Admin from "@/views/Admin.vue";
import Officer from "@/views/Officer.vue";
import test from "@/views/test.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      component: About
    },
    {
      path: "/contact",
      name: "contact",
      component: Contact
    },
    {
      path: "/hotelviewing",
      name: "hotelviewing",
      component: HotelViewing
    },
    {
      path: "/forms",
      name: "forms",
      component: Forms
    },
    {
      path: "/superadmin",
      name: "superadmin",
      component: SuperAdmin
    },
    {
      path: "/admin",
      name: "admin",
      component: Admin
    },
    {
      path: "/officer",
      name: "officer",
      component: Officer
    },
    {
      path: "/test",
      name: "test",
      component: test
    }
  ]
});
