import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import Cookies from "js-cookie";

Vue.use(Vuex);

export default new Vuex.Store({
  // strict:true,
  state: {
    VUE_APP_API: "http://localhost:3000",
    currentLocation: {
      lat: null,
      lng: null
    },
    changedLocation: {
      lat: null,
      lng: null
    },
    districtDetails: [],
    circleDetails: [],
    hotelDetails: [],
    nearestHotelsToMe: [],
    currentHotel: [],
    topRatedHotels: [],
    userDetails: [],
    complaintDetails: [],
    inspectionFormDetails: [],
    inspectionDetails: [],
    suggestionDetails: [],
    notificationDetails: [],
    loggedUserDetails: [],
    user_id: null,
    user_name: null,
    user_type: null,
    object: {},
    inspection_data: null,
    fullDetails: {}
  },
  plugins: [
    createPersistedState({
      object: {},
      paths: [
        "currentLocation",
        "changedLocation",
        "VUE_APP_API",
        "nearestHotelsToMe",
        "currentHotel",
        "topRatedHotels",
        "loggedUserDetails",
        "fullDetails",
        "inspection_data",
        "user_type",
        "object",
        "user_name",
        "user_id"
      ],
      getItem: key => Cookies.get(key),
      setItem: (key, value) =>
        Cookies.set(key, value, { expires: 3, secure: true }),
      removeItem: key => Cookies.remove(key)
    })
  ],

  mutations: {
    locationPush(state, payload) {
      state.currentLocation = {
        lat: payload.lat,
        lng: payload.lng
      };
    },
    pushTopRatedHotels(state, payload) {
      state.topRatedHotels = payload;
    },
    pushCurrentHotel(state, payload) {
      state.currentHotel = payload;
    },
    searchedLocationPush(state, payload) {
      state.changedLocation = {
        lat: payload.lat,
        lng: payload.lng
      };
    },
    getDistrict(state, payload) {
      state.districtDetails = payload;
    },
    getCircle(state, payload) {
      state.circleDetails = payload;
    },
    getHotel(state, payload) {
      state.hotelDetails = payload;
    },
    pushNearestHotels(state, payload) {
      state.nearestHotelsToMe = payload;
    },
    getUser(state, payload) {
      state.userDetails = payload;
    },
    getComplaint(state, payload) {
      state.complaintDetails = payload;
    },
    getInspectionFormData(state, payload) {
      state.inspectionFormDetails = payload;
    },
    getInspectionDetails(state, payload) {
      state.inspectionDetails = payload;
    },
    getSuggestionDetails(state, payload) {
      state.suggestionDetails = payload;
    },
    getNotifications(state, payload) {
      state.notificationDetails = payload;
    },
    getLoggedUserDetails(state, payload) {
      state.loggedUserDetails = payload;
    },
    assign(state, payload) {
      state.user_id = payload.id;
      state.user_name = payload.name;
      state.user_type = payload.type;
      state.object = payload;
      state.fullDetails = payload.full;
    },
    inspection(state, payload) {
      state.inspection_data = payload.inspection_data;
    }
  },
  actions: {
    // getUsers(){
    //   this.$http.get(state.VUE_APP_API+'/api/user_details')
    //     .then(function(res){
    //       state.districtDetails = res.data
    //     })
    // }
  },
  getters: {
    checkNearestHotelsToMe: state => () => state.nearestHotelsToMe
  }
});
