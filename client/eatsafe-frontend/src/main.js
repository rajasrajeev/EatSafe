import Vue from "vue";
import mainpage from "./mainpage.vue";
import router from "./router";
import store from "./store";
import VueResource from "vue-resource";
// import {ApiCalls} from '@/mixins/ApiCalls.js';

Vue.use(VueResource);
Vue.config.productionTip = false;

const bus = new Vue();
export default bus;

new Vue({
  router,
  store,
  // mixins:[ApiCalls],
  render: h => h(mainpage)
}).$mount("#app");

// console.log = function() {}