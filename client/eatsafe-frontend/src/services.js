import { resolve } from "url";
import Vue from "vue";
import VueResource from "vue-resource";
import axios from "axios";

Vue.use(VueResource);
export default class LoopbackResource {
  getDistricts() {
    var districtDetails = [];
    return new Promise(resolve => {
      axios
        .get(process.env.VUE_APP_URL + "/api/list_of_districts")
        .then(function(res) {
          districtDetails = res.data;
          resolve({
            value: districtDetails
          });
          // console.log("in mixin from store circle ==>",store.state.circleDetails)
        })
        .catch(function(error) {
          console.log("Error When Fetching circles details :", error);
        });
    });
  }
  getCircles() {
    var circleDetails = [];
    return new Promise(resolve => {
      axios
        .get(process.env.VUE_APP_URL + "/api/circles_details")
        .then(function(res) {
          //   store.commit('getCircle',res.data)
          circleDetails = res.data;
          resolve({
            value: circleDetails
          });
          // console.log("in mixin from store circle ==>",store.state.circleDetails)
        })
        .catch(function(error) {
          console.log("Error When Fetching circles details :", error);
        });
    });
  }
  getHotels() {
    var hotelDetails = [];
    return new Promise(() => {
      console.log(
        "hotelDetails",
        process.env.VUE_APP_URL + "/api/hotel_details"
      );
      axios
        .get(process.env.VUE_APP_URL + "/api/hotel_details")
        .then(function(res) {
          //   store.commit('getHotel',res.data)
          hotelDetails = res.data;
          resolve({
            value: hotelDetails
          });
          // console.log("in mixin from store hotel ==>",store.state.hotelDetails)
        })
        .catch(function(error) {
          console.log("Error When Fetching hotel details :", error);
        });
    });
  }
  getUsers() {
    var userDetails = [];

    return new Promise(() => {
      console.log("userDetails", process.env.VUE_APP_URL + "/api/user_details");
      axios
        .get(process.env.VUE_APP_URL + "/api/user_details")
        .then(function(res) {
          //   store.commit('getUser',res.data)
          userDetails = res.data;
          resolve({
            value: userDetails
          });
          // console.log("in mixin from store user ==>",store.state.userDetails)
        })
        .catch(function(error) {
          console.log("Error When Fetching user details :", error);
        });
    });
  }
  getComplaints() {
    var complaintDetails = [];
    return new Promise(() => {
      console.log(
        "complaintDetails",
        process.env.VUE_APP_URL + "/api/complaint_details_from_publics"
      );
      axios
        .get(process.env.VUE_APP_URL + "/api/complaint_details_from_publics")
        .then(function(res) {
          //   store.commit('getComplaint',res.data)
          complaintDetails = res.data;
          resolve({
            value: complaintDetails
          });
          // console.log("in mixin from store  complaint==>",store.state.complaintDetails)
        })
        .catch(function(error) {
          console.log("Error When Fetching complaint details :", error);
        });
    });
  }
  getInspectionFormDetails() {
    var inspectionFormDetails = [];
    return new Promise(() => {
      axios
        .get(process.env.VUE_APP_URL + "/api/inspection_form_data")
        .then(function(res) {
          //   store.commit('getInspectionFormData',res.data)
          inspectionFormDetails = res.data;
          resolve({
            value: inspectionFormDetails
          });
          // console.log("in mixin from store inspection form ==>",store.state.inspectionFormData)
        })
        .catch(function(error) {
          console.log(
            "Error When Fetching complaint form data details :",
            error
          );
        });
    });
  }
  getInspectionDetails() {
    var inspectionDetails = [];
    return new Promise(() => {
      axios
        .get(process.env.VUE_APP_URL + "/api/inspection_details")
        .then(function(res) {
          //   store.commit('getInspectionDetails',res.data)
          inspectionDetails = res.data;
          resolve({
            value: inspectionDetails
          });
          // console.log("in mixin from store inspection ==>",store.state.inspectionDetails)
        })
        .catch(function(error) {
          console.log(
            "Error When Fetching complaint form data details :",
            error
          );
        });
    });
  }
  getSuggestionDetails() {
    var suggestionDetails = [];
    return new Promise(() => {
      axios
        .get(process.env.VUE_APP_URL + "/api/suggestion_details_from_publics")
        .then(function(res) {
          //   store.commit('getSuggestionDetails',res.data)
          suggestionDetails = res.data;
          resolve({
            value: suggestionDetails
          });
          // console.log("in mixin from store suggestion ==>",store.state.suggestionDetails)
        })
        .catch(function(error) {
          console.log(
            "Error When Fetching complaint form data details :",
            error
          );
        });
    });
  }
  getNotifications() {
    var notificationDetails = [];
    return new Promise(() => {
      axios
        .get(process.env.VUE_APP_URL + "/api/notifications")
        .then(function(res) {
          //   store.commit('getNotifications',res.data)
          notificationDetails = res.data;
          resolve({
            value: notificationDetails
          });
          // console.log("in mixin from store notification ==>",store.state.notificationDetails)
        })
        .catch(function(error) {
          console.log(
            "Error When Fetching complaint form data details :",
            error
          );
        });
    });
  }
}
