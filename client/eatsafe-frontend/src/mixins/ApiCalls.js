"use strict";

import store from "@/store.js";
import { resolve } from "path";

export const ApiCalls = {
  data() {
    return {
      districtDetails: {
        type: String,
        twoWay: true
      },
      circleDetails: {
        type: String,
        twoWay: true
      },
      hotelDetails: {
        type: String,
        twoWay: true
      },
      userDetails: {
        type: String,
        twoWay: true
      },
      complaintDetails: {
        type: String,
        twoWay: true
      },
      inspectionFormDetails: {
        type: String,
        twoWay: true
      },
      inspectionDetails: {
        type: String,
        twoWay: true
      },
      suggestionDetails: {
        type: String,
        twoWay: true
      },
      notificationDetails: {
        type: String,
        twoWay: true
      },
      currentLocation: {
        type: Object,
        twoWay: true
      },
      hotels: {
        type: Object,
        twoWay: true
      },
      topRatedHotels: {
        type: Object,
        twoWay: true
      }
    };
  },
  created() {
    this.getDistricts();
    this.getCircles();
    this.getHotels();
    this.getUsers();
    this.getComplaints();
    this.getInspectionFormData();
    this.getInspectionDetails();
    this.getSuggestionDetails();
    this.getNotifications();
  },
  methods: {
    getDistricts() {
      return new Promise(() => {
        this.$http
          .get(process.env.VUE_APP_URL + "/api/list_of_districts")
          .then(function(res) {
            store.commit("getDistrict", res.data);
            this.districtDetails = res.data;
            return this.districtDetails;
            // console.log("in mixin from store circle ==>",store.state.circleDetails)
          })
          .catch(function(error) {
            console.log("Error When Fetching circles details :", error);
          });
      });
    },
    getCircles() {
      return new Promise(() => {
        this.$http
          .get(process.env.VUE_APP_URL + "/api/circles_details")
          .then(function(res) {
            store.commit("getCircle", res.data);
            this.circleDetails = res.data;
            // console.log("in mixin from store circle ==>",store.state.circleDetails)
          })
          .catch(function(error) {
            console.log("Error When Fetching circles details :", error);
          });
      });
    },
    getHotels() {
      return new Promise(() => {
        this.$http
          .get(process.env.VUE_APP_URL + "/api/hotel_details")
          .then(function(res) {
            store.commit("getHotel", res.data);
            this.hotelDetails = res.data;
            // console.log("in mixin from store hotel ==>",store.state.hotelDetails)
          })
          .catch(function(error) {
            console.log("Error When Fetching hotel details :", error);
          });
      });
    },
    getUsers() {
      return new Promise(() => {
        this.$http
          .get(process.env.VUE_APP_URL + "/api/user_details")
          .then(function(res) {
            store.commit("getUser", res.data);
            this.userDetails = res.data;
            // console.log("in mixin from store user ==>",store.state.userDetails)
          })
          .catch(function(error) {
            console.log("Error When Fetching user details :", error);
          });
      });
    },
    getComplaints() {
      return new Promise(() => {
        this.$http
          .get(process.env.VUE_APP_URL + "/api/complaint_details_from_publics")
          .then(function(res) {
            store.commit("getComplaint", res.data);
            this.complaintDetails = res.data;
            // console.log("in mixin from store  complaint==>",store.state.complaintDetails)
          })
          .catch(function(error) {
            console.log("Error When Fetching complaint details :", error);
          });
      });
    },
    getInspectionFormData() {
      return new Promise(() => {
        this.$http
          .get(process.env.VUE_APP_URL + "/api/inspection_form_data")
          .then(function(res) {
            store.commit("getInspectionFormData", res.data);
            this.inspectionFormDetails = res.data;
            // console.log("in mixin from store inspection form ==>",store.state.inspectionFormData)
          })
          .catch(function(error) {
            console.log(
              "Error When Fetching complaint form data details :",
              error
            );
          });
      });
    },
    getInspectionDetails() {
      return new Promise(() => {
        this.$http
          .get(process.env.VUE_APP_URL + "/api/inspection_details")
          .then(function(res) {
            store.commit("getInspectionDetails", res.data);
            this.inspectionDetails = res.data;
            // console.log("in mixin from store inspection ==>",store.state.inspectionDetails)
          })
          .catch(function(error) {
            console.log(
              "Error When Fetching complaint form data details :",
              error
            );
          });
      });
    },
    getSuggestionDetails() {
      return new Promise(() => {
        this.$http
          .get(process.env.VUE_APP_URL + "/api/suggestion_details_from_publics")
          .then(function(res) {
            store.commit("getSuggestionDetails", res.data);
            this.suggestionDetails = res.data;
            // console.log("in mixin from store suggestion ==>",store.state.suggestionDetails)
          })
          .catch(function(error) {
            console.log(
              "Error When Fetching complaint form data details :",
              error
            );
          });
      });
    },
    getNotifications() {
      return new Promise(() => {
        this.$http
          .get(process.env.VUE_APP_URL + "/api/notifications")
          .then(function(res) {
            store.commit("getNotifications", res.data);
            this.notificationDetails = res.data;
            // console.log("in mixin from store notification ==>",store.state.notificationDetails)
          })
          .catch(function(error) {
            console.log(
              "Error When Fetching complaint form data details :",
              error
            );
          });
      });
    }
  }
};
