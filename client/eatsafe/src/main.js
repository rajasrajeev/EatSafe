import Vue from 'vue'
import mainpage from './mainpage.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Carousel3d from 'vue-carousel-3d'
import Vuex from 'vuex'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueResource from 'vue-resource'

Vue.use(VueResource)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAUJ4NBY-BWzVd1B4FykY_vzzU3yHLCsHE',
    libraries: 'places' // necessary for places input
  }
})

Vue.use(Vuex)

Vue.use(Carousel3d)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(mainpage)
}).$mount('#app')
