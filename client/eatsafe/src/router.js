import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import contact from './views/contact.vue'
import superadmin from './views/superadmin.vue'
import admin from './views/admin.vue'
import officer from './views/officer.vue'
import test from './views/test.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/superadmin',
      name: 'superadmin',
      component: superadmin
    },
    {
      path: '/admin',
      name: 'admin',
      component: admin
    },
    {
      path: '/officer',
      name: 'officer',
      component: officer

    },
    {
      path: '/contact',
      name: 'contact',
      component: contact
    },
    {
      path: '/test',
      name: 'test',
      component: test
    }
  ]
})
