import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user_id: null,
    user_name: null,
    user_type: null,
    object: {},
    inspection_data: null,
    fullDetails:{}
  },
  mutations: {
    assign (state, payload) {
      state.user_id = payload.id
      state.user_name = payload.name
      state.user_type = payload.type
      state.object = payload
      state.fullDetails = payload.full
    },
    inspection (state, payload) {
      state.inspection_data = payload.inspection_data
    }
  },
  actions: {

  }
})
