'use strict';

module.exports = function(Activitylog) {
    Activitylog.pipeline = function (cb) {
        //Get the MongoDB Connection
        var mongodbConnection = Activitylog.dataSource.connector.db;
        if (!mongodbConnection) {
            // may not be connected yet, you might have to do that manually:
            // (careful! this is asynchronous)
            Activitylog.dataSource.connect(function(err, db) {
                mongodbConnection = db;
            });
        }
        console.log("mongodbconnection",mongodbConnection)
        // do whatever you need to with the mongo connection...
        cb(result);
    };

    // ... other stuff
};
